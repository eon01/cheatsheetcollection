# CheatSheetCollection

![alt text](https://www.researchgate.net/profile/Henrique_Gaspar/publication/325361376/figure/fig2/AS:630135694831618@1527247465316/DevOps-as-culture-in-software-development-Kornilova-2018.png)

![](https://cookbook.fortinet.com/wp-content/uploads/sysadmin_notes-logo-2.gif)

# List of contents:
- ### [Containers](Containers/List.md)
- ### [Agile](Agile/List.md)
- ### [DevOps Automation](DevOps Automation/List.md)
- ### [DevOps Linux](DevOps Linux/List.md)
- ### [DevOps Services](DevOpsServices/List.md)
- ### [Languages](Languages/List.md)
- ### [Network](Network/List.md)
- ### [Scripting](Scripts/List.md)
- ### [Security](Security/List.md)
- ### [Solutions](Solutions/List.md)
- ### [Virtualization](Virtualization/List.md)
- ### [Windows](Windows/List.md)
- ### [Glib Examples](Glib Examples/List.md)
- ### [Go Examples](Go Examples/List.md)
- ### [Javascript Examples](Javascript Examples/List.md)
- ### [PHP Examples](PHP Examples/List.md)
- ### [Python Examples](Python Examples/List.md)
- ### [Programming Language Examples Alike Cookbook](Programming Language Examples Alike Cookbook/List.md)


# Slack groups:

|   | Name               | URL                                   |
|---|--------------------|---------------------------------------|
|   | devopsengineers    | https://devopsengineers.slack.com/    |
|   | securityhq         | https://devopsengineers.slack.com/    |
|   | redhat-admins      | https://devopsengineers.slack.com/    |
|   | devopslinks        | https://devopsengineers.slack.com/    |
|   | devspl             | https://devspl.slack.com/             |
|   | chefcommunity      | https://chefcommunity.slack.com/      |
|   | openstackcommunity | https://openstackcommunity.slack.com/ |
|   | puppetcommunity    | https://puppetcommunity.slack.com/    |
|   | winadmins          | https://winadmins.slack.com/          |